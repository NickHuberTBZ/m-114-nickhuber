# Auftrag 1

Jeder denkt für sich ein Wort mit ca. 15 Buchstaben aus und erstellt dazu die Huffman-Codetabelle und das entsprechend komprimierte Wort in HEX-Darstellung. Nun werden die Codes inklusive der Codetabelle gegenseitig ausgetauscht. Kann ihr Partner ihr gewähltes Wort richtig dekomprimieren? 


- Wort: "Kommunikationspartner"

**Huffman-Codetabelle:**
```
K: 000
o: 01
m: 001
u: 100
n: 101
i: 110
k: 1110
a: 11110
t: 111110
s: 1111110
p: 11111110
r: 11111111
```

**Komprimiertes Wort (in Binär):**
```
1110001111011100111010011011100101111101001101111001110100111110111011111110111101111001111111101111101111
```

**Komprimiertes Wort (in HEX):**
```
E3EED9D6F2DFE9F7BBF7EF7BF
```

# Auftrag 2

Sie erhalten diesen RL-Code: 
010100011110010010010010010010010010010110010110010010010010010010010010001 
Folgendes ist ihnen dazu bekannt: Es handelt sich um eine quadratische Schwarz-Weiss-Rastergrafik mit einer Kantenlänge von 8 Pixel. Es wird links oben mit der Farbe Weiss begonnen. Eine Farbe kann sich nicht mehr als siebenmal wiederholen. Zeichnen sie die Grafik auf. Was stellt sie dar?

- Das Bild zeigt den Grossbuchstaben A

# Auftrag 3

Erstellen sie die LZW-Codierung für das Wort «ANANAS» und überprüfen sie mit der Dekodierung ihr Resultat. Danach versuchen sie den erhaltenen LZW-Code «ERDBE<256>KL<260>» zu dekomprimieren.

- LZW-Codierung für «ANANAS»:  AN«256»AS
- LZW-Decodierung für «ERDBE<256>KL<260>»: ERDBEERKLEE

# Auftrag 4

Wir wollen die Effizienz bei der ZIP-Komprimierung untersuchen. Dazu sollen sie ASCII-Textdateien erstellen. 

Die erste enthält 10, die zweite 100, die dritte 1000, die vierte 10'000 und die fünfte 100'000 ASCII-Zeichen. 

**ASCII-Textdateien (ABCDEFGHIJ)**
- Test10 : 128
- Test100 : 135
- Test1000 : 143
- Test10000 : 172
- Test100000 : 348

Achten sie darauf, dass die Zeichen möglichst zufällig gewählt werden. Auf dem Internet findet man entsprechende Textgeneratoren. 

- Random100000 : 4141 (Zufallsabfolge)
- CHAR100000   : 241 (Alles Buchstabe A)

Kopieren sie jede dieser fünf Textdateien in eine eigene ZIP-Datei. In der Folge erhalten sie fünf ZIP-Dateien. 

**Bilddateien:**
- Original: 1000 x 1000 x 3 = 3'000'000

- TestLo.jpg : 32810
- TestLo.zip : 17590
- TestHi.jpg : 271201
- TestHi.zip : 243119

# Auftrag 5 

Erstellen sie die BWT-Transformation für das Wort ANANAS und überprüfen sie mit der Rücktransformation ihr Resultat. 

- BWT-Transformation für ANANAS: SNNAAA1

Sie erhalten den Code IICRTGH6 in der Burrows-Wheeler-Transformation. Welches Wort verbirgt sich dahinter? 

- BWT-Rücktransformation IICRTGH6: RICHTIG

# Reflexion

**Unterrichtsinhalte und Ziele:**

Mein Ziel war es die Bildkomprimierung zu verstehen und einige Aufgaben dazu zu lösen. Das **Hauptziel** ist natürlich eine gute Note in der Prüfung zu schreiben

**Unterrichtsresultate:**

Die Unterrichtsresultate sind hier auf meinem GitLab.

**Probleme/Knacknüsse:**

Ich hatte Probleme beim verstehen der Bildkomprimierung

**Neue Applikationen, Werkzeuge, Kommandos, etc.:**

--Keine Neue Applikationen, Werkzeuge, Kommandos oder weiteres genutzt--

**Offene Fragen:**
Ich habe folgende Fragen:

- [X] Wie kommt man genau auf die verschiedenen Komprimierungen?

- [X] Bildkomprimierung wie kommt der Rythmus zu Stande?
