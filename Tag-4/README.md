# TAG 4 gehört zum TAG 3

- [Verlinkung](/Tag-3)

# Kapitel-C: BILDER CODIEREN UND KOMPRIMIEREN

## Aufgabe: Bestimmen Sie die Farben für die folgenden RGB-Farbcodes (in HEX). Nutzen Sie den RGB-Farbenmixer:

- `#FF0000` entspricht der Farbe ROT
- `#00FF00` entspricht der Farbe GRÜN
- `#0000FF` entspricht der Farbe BLAU
- `#FFFF00` entspricht der Farbe GELB (Komplementärfarbe RGB-CMYK)
- `#00FFFF` entspricht der Farbe CYAN (Komplementärfarbe RGB-CMYK)
- `#FF00FF` entspricht der Farbe MAGENTA (Komplementärfarbe RGB-CMYK)
- `#000000` entspricht der Farbe SCHWARZ
- `#FFFFFF` entspricht der Farbe WEISS
- `#00BC00` entspricht der Farbe DUNKELGRÜN

## Aufgabe: Bestimmen Sie die Farben für die folgenden prozentualen CMYK-Angaben. Nutzen Sie den CMYK-Farbenmixer:

- C:0%, M:100%, Y:100%, K:0% entspricht der Farbe ROT
- C:100%, M:0%, Y:100%, K:0% entspricht der Farbe GRÜN
- C:100%, M:100%, Y:0%, K:0% entspricht der Farbe BLAU
- C:0%, M:0%, Y:100%, K:0% entspricht der Farbe SCHWARZ (wenn Keyfarbe=Schwarz)
- C:100%, M:0%, Y:0%, K:0% entspricht der Farbe CYAN
- C:0%, M:100%, Y:0%, K:0% entspricht der Farbe MAGENTA
- C:100%, M:100%, Y:100%, K:0% entspricht der Farbe SCHWARZ
- C:0%, M:0%, Y:0%, K:100% entspricht der Farbe SCHWARZ
- C:0%, M:0%, Y:0%, K:0% entspricht der Farbe WEISS
- C:0%, M:46%, Y:38%, K:22% entspricht der Farbe ALTROSA

## Aufgabe mehr Video Formate usw.

### Video Codecs:
1. **H.264 (AVC):** Ein weit verbreiteter Videocodec für High Definition (HD) und Blu-ray.
2. **H.265 (HEVC):** Der Nachfolger von H.264, der eine verbesserte Kompression bietet.
3. **VP9:** Ein von Google entwickelter, lizenzfreier Videocodec mit hoher Kompressionsleistung.
4. **AV1:** Ein offener, lizenzfreier Videocodec, der von der Alliance for Open Media entwickelt wurde.
5. **MPEG-2:** Ein älterer Standard, der oft für DVDs verwendet wird.
6. **MPEG-4:** Enthält verschiedene Komponenten, darunter Video und Audio. Häufig für Streaming und Videokonferenzen verwendet.

### Audio Codecs:
1. **AAC (Advanced Audio Coding):** Ein verlustbehafteter Audiocodec, der häufig in MP4-Dateien für Musik und Videos verwendet wird.
2. **MP3 (MPEG-1 Audio Layer III):** Ein weit verbreiteter verlustbehafteter Audiocodec für Musik.
3. **Opus:** Ein offener und vielseitiger Audiocodec, der für Echtzeitkommunikation und Streaming optimiert ist.
4. **FLAC (Free Lossless Audio Codec):** Ein verlustfreier Audiocodec, der eine hochwertige Audiokompression bietet.
5. **WAV (Waveform Audio File Format):** Ein unkomprimiertes Audioformat, das oft für die Speicherung von hochwertigen Audiodateien verwendet wird.
6. **Dolby Digital (AC-3):** Ein häufig verwendeter Audiocodec für Surround Sound in Film und Video.

### Containerformate:
1. **MP4 (MPEG-4 Part 14):** Ein weit verbreitetes Containerformat für Audio- und Videodateien.
2. **MKV (Matroska):** Ein offenes Containerformat, das verschiedene Video-, Audio- und Untertitelformate unterstützt.
3. **AVI (Audio Video Interleave):** Ein älteres Containerformat, das von Microsoft entwickelt wurde und verschiedene Videocodecs unterstützt.
4. **MOV (QuickTime):** Ein von Apple entwickeltes Containerformat, das oft für Video- und Audiodateien auf Mac-Plattformen verwendet wird.
5. **FLV (Flash Video):** Ein von Adobe entwickeltes Containerformat, das häufig für das Streaming von Videos im Internet verwendet wird.
6. **WEBM:** Ein offenes Containerformat für Webvideos, das VP9-Video und Opus-Audio unterstützt.



- Berechnen Sie den Speicherbedarf für ein unkomprimiertes Einzelbild im HD720p50-Format bei einer True-Color-Farbauflösung. 
  - `1280 x 720 x 3Byte = 2’764’800 Byte`

- Welchen Speicherbedarf hat das Video aus der vorangegangenen Aufgabe bei einer Spieldauer von 3 Minuten? 
  - `2’764’800B x 50 x 180 = 24’883’200’000 Byte`


- Sie haben ein 30-Zoll-Display (Diagonale) im Format 16:10 und 100ppi erworben. Was ist die Pixelauflösung horizontal und vertikal? 
  - Pythagoras: `302 = (16x)2 + (10x)2`
  - `900 = 256x2 + 100x2` oder `900 = 356x2` ergibt für `x=1.5899`
  - Somit Breite=`16 x 1.5899 = 25.44`  Höhe=`10 x 1.5899 = 15.899`
  - `100ppi` bedeutet 100 Pixel pro Inch: Somit: Breite=`2544 Pixel`   Höhe=`1590 Pixel`

- Sie drucken ein quadratisches Foto mit einer Kantenlänge von 2000 Pixel mit 600dpi. Wie groß in cm wird dieses? 
  - `2000 Pixel mit 600 Dots per Inch: 2000/600 = 3.333 Inch` oder `8.47 cm`


## Verlustbehaftete Komprimierung von Bildern und Videos

- RGB 255/255/255 ergibt in YCbCr 1-0-0
- RGB 0/0/0 ergibt in YCbCr 0-0-0
- Y:1, Cb:0, Cr:0 entspricht der Farbe WEISS
- Y:0, Cb:0, Cr:0 entspricht der Farbe SCHWARZ
- Y:0, Cb:0.5, Cr:0 entspricht der Farbe ROT
- Y:0, Cb:-0.5, Cr:0 entspricht der Farbe GRÜN
- Y:0, Cb:0, Cr:0.5 entspricht der Farbe BLAU
- Y:0, Cb:0, Cr:-0.5 entspricht der Farbe GRÜN
- Y:0.3, Cb:0.5, Cr:-0.17 entspricht der Farbe ROT

### Aufgaben

- Was ist der Unterschied zwischen dem Interlaced Mode und dem Progressive Mode? 
  - Interlaced: Halbbilder (Ungerade Zeilen / Gerade Zeilen)
  - Progressiv: Ganze Bilder

- Ein RGB-Farbbild benutzt nur die Farbe Weiss als Hintergrund und ein Hellblau mit folgenden Werten: R=33, G=121, B=239 (8 Bit pro Farbkanal). Das Bild soll in ein Graustufenbild umgewandelt werden. Berechnen Sie den für das Hellblau entsprechende Grauwert. (8 Bit pro Farbkanal) 
  - Rot: `33 x 0.3 = 9.9` / Grün: `121 x 0.6 = 72.6` / Blau: `239 x 0.1 = 23.9` Summe=`106.4`
  - Somit Graustufenwert=`106`

- Was versteht man unter Artefakten und welche kennen Sie? 
  - Zwei Beispiele dazu:
    - Der Moiré-Effekt ist ein optischer Effekt, bei dem durch Überlagerung von regelmäßigen Rastern ein wiederum periodisches Raster entsteht, das spezielle Strukturen aufweist, die in keinem der Einzel-Muster vorhanden sind und bei Veränderung der Überlagerungsweise variieren.
    - Strukturen im Bild die von z.B. einer Komprim


# Reflexion

**Unterrichtsinhalte und Ziele:**

Mein Ziel war es einfach alle Aufgaben zu lösen, auch diese welche mit den Bildern zu lösen waren.

**Unterrichtsresultate:**

Die Unterrichtsresultate sind hier auf meinem GitLab.

**Probleme/Knacknüsse:**

Ich hatte Probleme beim editieren der Bilder.

**Neue Applikationen, Werkzeuge, Kommandos, etc.:**

--Keine Neue Applikationen, Werkzeuge, Kommandos oder weiteres genutzt--

**Offene Fragen:**
Ich habe folgende Fragen:

- [X] Keine offenen Fragen
