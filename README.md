# M114 - Nick Huber

### Wie ist das Git aufgebaut?

Das Git ist so aufgebaut, dass man immer zwischen den Tagen einfach durch findet. Unten finden sie die Verlinkung zu den verschiedenen Tagen.

### Dokumentationen zu den verschiedenen Tagen

- **[Dokumentation - Daten Codieren/Teil 1](Tag-1)**
- **[Dokumentation - Daten Codieren/Teil 2](Tag-2)**
- **[Dokumentation - Daten Komprimierung](Tag-3)**
- **[Dokumentation - Bilder Komprimierung](Tag-4)**
- **[Dokumentation - Umwandlung Dokumente](Tag-5)**
- **[Dokumentation - Kyrptografie](Tag-6)**


```html
<!-- Task Creation Modal -->
                    <div class="modal fade" id="createTask" tabindex="-1" aria-labelledby="createTaskLabel" aria-hidden="true">
                        <div class="modal-dialog modal-dialog-centered">
                            <div class="modal-content">
                                <div class="modal-header">
                                    <h5 class="modal-title" id="createTaskLabel">Create New Task</h5>
                                    <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
                                </div>
                                <div class="modal-body">
                                    <form id="taskForm">
                                        <div class="form-group mb-3">
                                            <label for="taskName">Task Name</label><small><i> *required</i></small>
                                            <input type="text" class="form-control" id="taskName" placeholder="Enter task name" required>
                                        </div>
                                        <div class="form-group mb-3">
                                            <label for="taskDescription">Task Description</label><small><i> *required</i></small>
                                            <textarea class="form-control" id="taskDescription" rows="3" placeholder="Enter task description" required></textarea>
                                        </div>
                                        <div class="form-group mb-3">
                                            <label for="startDate">Starting Date</label><small><i> *required</i></small>
                                            <input type="text" class="form-control" id="startDate" placeholder="dd:mm:yyyy" required>
                                        </div>
                                        <div class="form-group mb-3">
                                            <label for="assignUser">Assign to User</label>
                                            <select class="form-control" id="assignUser" required>
                                                <option selected>Myself</option>
                                                <!-- Add more options as needed -->
                                            </select>
                                        </div>
                                        <div class="form-group mb-3">
                                            <label for="createdBy">Task Created by</label><small><i> *required</i></small>
                                            <select class="form-control" id="createdBy" required>
                                                <option selected>Myself</option>
                                                <!-- Add more options as needed -->
                                            </select>
                                        </div>
                                    </form>
                                </div>
                                <div class="modal-footer">
                                    <button type="button" class="btn btn-secondary" data-bs-dismiss="modal">Close</button>
                                    <button type="submit" form="taskForm" class="btn btn-primary">Save changes</button>
                                </div>
                            </div>
                        </div>
                    </div>
                    
                    <a class="nav-link bi-code" href="#"> Source Code</a>
                    <a class="nav-link bi-github" href="#"> GitHub</a>
                    <a class="nav-link bi-person-fill" href="#"> Creators</a>

                </nav>

            </div>
            
            <!-- New section for task deletion, similar UI integration as the creation -->
            <div class="p-2 flex-fill">
                <button class="btn btn-danger" data-bs-toggle="modal" data-bs-target="#deleteTaskModal">Delete Task</button>
                
                <!-- Task Deletion Modal -->
                <div class="modal fade" id="deleteTaskModal" tabindex="-1" aria-labelledby="deleteTaskLabel" aria-hidden="true">
                    <div class="modal-dialog modal-dialog-centered">
                        <div class="modal-content">
                            <div class="modal-header">
                                <h5 class="modal-title" id="deleteTaskLabel">Delete Task</h5>
                                <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
                            </div>
                            <div class="modal-body">
                                <form id="deleteTaskForm">
                                    <div class="form-group mb-3">
                                        <label for="taskIdToDelete">Task ID</label>
                                        <input type="number" class="form-control" id="taskIdToDelete" placeholder="Enter Task ID to delete" required>
                                    </div>
                                    <button type="submit" class="btn btn-danger">Delete Task</button>
                                </form>
                            </div>
                        </div>
                    </div>
                </div>
```