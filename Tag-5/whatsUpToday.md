# Whats up today? 12.12.2023
    
- Umwandlung von Analog zu Digital (AD-Wandler)
- Audiobeispiele, Frequency-sweep
- Verlustbehaftete Komprimierung bei Bild und Video
- Luminanz & Chrominanz, Subsampling
- JPG-Komprimierung mit DCT
- GOP-Sequenz
