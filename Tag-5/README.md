### Analog/Digital Umwandlung:

**1. Warum benötigt man AD-Wandler?**
   - Die ALU im Mikroprozessor kann nur digitale Signale verarbeiten.

**2. Warum geht eine A/D-Wandlung immer mit einem Datenverlust einher?**
   - Je nach Samplingrate und Auflösung wird das analoge Signal zu einer treppenartigen Angelegenheit. Die Bereiche zwischen den Stufen bleiben unbekannt.

**3. Gibt eine höhere oder eine tiefere Samplingrate eine präzisere Abbildung des Originals? Begründen sie!**
   - Je höher die Samplingrate, desto näher kommt die digitale Abbildung dem Original. Das Abtast-Theorem besagt, dass die Samplingfrequenz der doppelten oberen Grenzfrequenz entsprechen sollte.

---

### RGB/YCbCr:

**4. Kann man durch die Bildumwandlung vom RGB- in den YCbCr-Farbraum Speicherplatz einsparen?**
   - Nein, es sind immer noch drei Kanäle mit z.B. 8 Bit Auflösung pro Kanal.

**5. Kann ein Beamer ein Bild im YCbCr-Farbbereich darstellen?**
   - Nein, der Beamer verwendet drei Farbkanäle im additiven Farbsystem (RGB).

**6. Wie rechnet man ein Farbbildes in ein Graustufenbild um?**
   - Die Farbe Grün hat 60% Anteil, Rot 30%, und Blau 10%.

**7. Warum hat bei der Umwandlung eines Farbbildes in ein Graustufenbild der Grünanteil am meisten Gewicht?**
   - Das menschliche Auge ist für Grün am empfindlichsten, was evolutionäre Gründe hat.

---

### Chroma-Subsampling:

**8. Warum verschlechtert sich die Bildschärfe von 4:1:1-Subsampling gegenüber 4:4:4-Subsampling nicht?**
   - Der Luminanzkanal (Graustufenkanal) bleibt gleich hoch aufgelöst.

**9. Ein quadratisches 24-Bit-RGB-Bild mit einer Kantenlänge von 1000 Pixel soll mit 4:1:1 unterabgetastet werden. Wie viel Speicherplatz wird damit eingespart?**
   - 4:1:1 benötigt die Hälfte des Speicherplatzes im Vergleich zum Original.

---

### JPG-Komprimierung:

**10. Was ist der erste Schritt bei der JPG-Komprimierung?**
   - Umwandlung von RGB in YCbCr, gefolgt von Subsampling.

**11. Führt die DCT-Transformation zu einer Datenreduktion?**
   - Nein, erst die Quantisierung und RLC führen zu Datenreduktion.

**12. Warum erhält man bei einer sehr starken Bildkomprimierung sogenannte Block-Artefakte?**
   - Die Unterteilung in 8x8-Pixel Blöcke führt dazu, dass sich Farben angleichen und als Quadrate wahrgenommen werden.

---

### Codec/Container:

**13. Was ist der Unterschied zwischen Intraframe- und Interframe-Komprimierung?**
   - Intraframe: Komprimierung innerhalb eines Bildes.
   - Interframe: Komprimierung über eine Bildserie.

**14. Bei welcher Filmsequenz bietet die Interframekomprimierung mehr Potential zur Datenreduzierung?**
   - a. 30 Sekunden Faultier auf Nahrungssuche, da sich die Bilder kaum unterscheiden.

**15. Sehen Sie Parallelen zwischen Datenbackupkonzepten und Interframe-Komprimierung?**
   - Ja, beide nutzen das Konzept der Redundanzreduktion, z.B. durch periodische Fullbackups (I-Frames).

**16. Was versteht man unter GOP25?**
   - Jedes 25. Bild ist ein komplettes Bild (I-Frame oder Schlüsselbild).

# Reflexion

**Unterrichtsinhalte und Ziele:**

Mein Ziel war es die Bildkomprimierung mit den verschiedenen Farben gut zu verstehen, dies habe ich eingermassen erreicht.

**Unterrichtsresultate:**

Die Unterrichtsresultate sind hier auf meinem GitLab.

**Probleme/Knacknüsse:**

Ich hatte keine Probleme die man nennen musste.

**Neue Applikationen, Werkzeuge, Kommandos, etc.:**

--Keine Neue Applikationen, Werkzeuge, Kommandos oder weiteres genutzt--

**Offene Fragen:**
Ich habe folgende Fragen:

- [X] keine offenen Fragen
